//
//  PencilPoint.swift
//  Yugen
//
//  Created by Misael Pérez Chamorro on 6/4/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

struct PencilLocation {
  var start: CGPoint
  var end: CGPoint
}

struct Pencil {
  var location: PencilLocation
  var strokeWith: CGFloat
  var color: CGColor
}
