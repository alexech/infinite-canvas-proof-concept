//
//  CenterView.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/12/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

class CenterView: UIView {

  @IBOutlet weak var hostScrollView: UIScrollView?
  @IBOutlet weak var topConstraint: NSLayoutConstraint?
  @IBOutlet weak var bottomConstraint: NSLayoutConstraint?
  @IBOutlet weak var leftConstraint: NSLayoutConstraint?
  @IBOutlet weak var rightConstraint: NSLayoutConstraint?

  internal var referenceCoordinates: (Int, Int) = (0, 0)
  internal var centreCoordinates: (Int, Int) = (Int.max, Int.max)
  internal var arbitraryLargeOffset: CGFloat = 1000000.0
  internal var gridSize: CGRect!

  internal var drawCenterView: UIView!
  internal var centerOffset: CGPoint = CGPoint.zero
  internal var allocateBoards: [GridView] = []
  internal var allocatePencils: [Pencil] = []

  private(set) var observingScrollview: Bool = false
  private var observation: NSKeyValueObservation?
  private var isContentOffsetFinish = false

  deinit {
    if observingScrollview {
      observation?.invalidate()
    }
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    defineScrollableArea()
    centreOurReferenceView()
    allocateInitialBoards()
    observeScrollview()
    initSetup()
  }

  private func defineScrollableArea() {
    guard let scrollview = hostScrollView else { return }
    topConstraint?.constant = arbitraryLargeOffset
    bottomConstraint?.constant = arbitraryLargeOffset
    leftConstraint?.constant = arbitraryLargeOffset
    rightConstraint?.constant = arbitraryLargeOffset
    gridSize = CGRect(x: 0, y: 0, width: scrollview.frame.size.width, height: scrollview.frame.size.height)
    scrollview.delegate = self
    scrollview.layoutIfNeeded()
  }

  private func centreOurReferenceView() {
    guard let scrollview = hostScrollView else { return }
    let xOffset = arbitraryLargeOffset - ((scrollview.frame.size.width - self.frame.size.width) * 0.5)
    let yOffset = arbitraryLargeOffset - ((scrollview.frame.size.height - self.frame.size.height) * 0.5)
    scrollview.setContentOffset(CGPoint(x: xOffset, y: yOffset), animated: false)
    self.isContentOffsetFinish = true
  }

  private func allocateInitialBoards() {
    if let scrollview = hostScrollView {
      adjustGrid(for: scrollview)
    }
  }

  private func allocateBoard(at boardCoordinates: (Int, Int)) {
    guard gridExists(at: boardCoordinates) == false else { return }
    let board = GridView(frame: self.frameForTile(at: boardCoordinates), coordinates: boardCoordinates)
    board.isUserInteractionEnabled = true
    board.backgroundColor = UIColor.clear
    self.allocateBoards.append(board)
    UIView.animate(withDuration: 0.1) {
      self.addSubview(board)
    }
  }

  private func initSetup() {
    drawCenterView = UIView()
    drawCenterView.frame = self.bounds
    drawCenterView.backgroundColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.05)
    self.addSubview(drawCenterView)
  }

  private func frameForTile(at coordinates: (Int, Int)) -> CGRect {
    let xIntOffset = coordinates.0 - referenceCoordinates.0
    let yIntOffset = coordinates.1 - referenceCoordinates.1
    let xOffset = gridSize.width * 0.5 + (gridSize.width * (CGFloat(xIntOffset) - 0.5))
    let yOffset = gridSize.height * 0.5 + (gridSize.height * (CGFloat(yIntOffset) - 0.5))
    return CGRect(x: xOffset, y: yOffset, width: gridSize.width, height: gridSize.height)
  }

  private func populateGridInBounds(lowerX: Int, upperX: Int, lowerY: Int, upperY: Int) {
    guard upperX > lowerX, upperY > lowerY else { return }
    var coordX = lowerX
    while coordX <= upperX {
      var coordY = lowerY
      while coordY <= upperY {
        allocateBoard(at: (coordX, coordY))
        coordY += 1
      }
      coordX += 1
    }
  }

  private func gridExists(at gridCoordinates: (Int, Int)) -> Bool {
    for grid in allocateBoards where grid.coordinates == gridCoordinates {
      return true
    }
    return false
  }

  private func clearGridOutsideBounds(lowerX: Int, upperX: Int, lowerY: Int, upperY: Int) {
    let gridsToProcess = allocateBoards
    for grid in gridsToProcess {
      let gridX = grid.coordinates.0
      let gridY = grid.coordinates.1
      if gridX < lowerX || gridX > upperX || gridY < lowerY || gridY > upperY {
        grid.removeFromSuperview()
        if let index = allocateBoards.index(of: grid) {
          allocateBoards.remove(at: index)
        }
      }
    }
  }

  private func observeScrollview() {
    guard observingScrollview == false,
      let scrollview = hostScrollView
      else { return }
    observation = scrollview.observe(\.contentOffset, changeHandler: { (scrollChangeView, _) in
      self.adjustGrid(for: scrollChangeView)
    })
    scrollview.delegate = self
    observingScrollview = true
  }

  internal func adjustGrid(for scrollview: UIScrollView) {
    let centre = computedCentreCoordinates(scrollview)
    guard centre != centreCoordinates else { return }
    self.centreCoordinates = centre
    guard gridSize.width > 0 else { return }
    let xTilesRequired = Int((gridSize.width) / (gridSize.width * 0.9))
    let yTilesRequired = Int((gridSize.height) / (gridSize.height * 0.9))
    let lowerBoundX = centre.0 - xTilesRequired
    let upperBoundX = centre.0 + xTilesRequired
    let lowerBoundY = centre.1 - yTilesRequired
    let upperBoundY = centre.1 + yTilesRequired
    populateGridInBounds(lowerX: lowerBoundX, upperX: upperBoundX,
                         lowerY: lowerBoundY, upperY: upperBoundY)
    clearGridOutsideBounds(lowerX: lowerBoundX, upperX: upperBoundX,
                           lowerY: lowerBoundY, upperY: upperBoundY)
  }

  internal func computedCentreCoordinates(_ scrollview: UIScrollView) -> (Int, Int) {
    guard gridSize.width > 0 else { return centreCoordinates }
    let contentOffset = scrollview.contentOffset
    let scrollviewSize = scrollview.frame.size
    let xOffset = -(self.center.x - (contentOffset.x + scrollviewSize.width * 0.5))
    let yOffset = -(self.center.y - (contentOffset.y + scrollviewSize.height * 0.5))
    let xIntOffset = Int((xOffset / gridSize.width).rounded())
    let yIntOffset = Int((yOffset / gridSize.height).rounded())
    return (xIntOffset + referenceCoordinates.0, yIntOffset + referenceCoordinates.1)
  }
}

extension CenterView: UIScrollViewDelegate {

  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if isContentOffsetFinish {
      self.coordinateScroll()
    }
  }
  
  func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
    self.readjustOffsets()
    NSObject.cancelPreviousPerformRequests(withTarget: self)
  }
}
