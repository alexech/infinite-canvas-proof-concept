//
//  GridView.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/12/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

class GridView: UIView {

  let verticalSpace: CGFloat = 11.5
  let horizontalSpace: CGFloat = 12
  let coordinates: (Int, Int)
  
  init(frame: CGRect, coordinates: (Int, Int)) {
    self.coordinates = coordinates
    super.init(frame: frame)
    self.backgroundColor = UIColor.clear
    addCoordinatesLabel()
  }

  required init?(coder aDecoder: NSCoder) {
    return nil
  }

  override func draw(_ rect: CGRect) {
    let path = UIBezierPath()
    let lines = self.numberOflines()
    for index in 1...lines {
      path.move(to: CGPoint(x: 5, y: CGFloat(index) * self.verticalSpace - 10.5))
      path.addLine(to: CGPoint(x: self.frame.width, y: CGFloat(index) * self.verticalSpace - 10.5))
    }
    path.lineWidth = 1.5
    let dashes: [CGFloat] = [0.001, path.lineWidth * self.horizontalSpace]
    path.setLineDash(dashes, count: dashes.count, phase: 0)
    path.lineCapStyle = CGLineCap.round
    UIColor.lightGray.setStroke()
    path.stroke()
  }

  private func addCoordinatesLabel() {
    let label = UILabel(frame: self.bounds)
    label.text = "\(coordinates)"
    label.font = UIFont.systemFont(ofSize: 24.0)
    label.textColor = UIColor.darkGray
    label.textAlignment = .center
    label.minimumScaleFactor = 0.5
    label.adjustsFontSizeToFitWidth = true
    self.addSubview(label)
  }
  
  func numberOflines() -> Int {
    let heigth = frame.height
    let lines = heigth / verticalSpace
    return Int(lines)
  }

}
