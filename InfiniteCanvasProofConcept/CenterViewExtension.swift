//
//  CenterViewExtension.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/12/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

extension CenterView {

  internal func coordinateScroll() {
    guard let scrollView = hostScrollView else { return }
    var centerPositionX: CGFloat = 0.0
    var centerPositionY: CGFloat = 0.0
    let offsetX = scrollView.contentOffset.x
    let offsetY = scrollView.contentOffset.y
    let largeOffset = self.arbitraryLargeOffset

    if offsetX > arbitraryLargeOffset {
      centerPositionX = offsetX - largeOffset
    }
    if offsetY > arbitraryLargeOffset {
      centerPositionY = offsetY - largeOffset
    }
    if offsetX < arbitraryLargeOffset {
      centerPositionX = -(largeOffset - offsetX)
    }
    if offsetY < arbitraryLargeOffset {
      centerPositionY = -(largeOffset - offsetY)
    }

    debugPrint("Center: \(centerPositionX),\(centerPositionY)")
    self.centerOffset = CGPoint(x: centerPositionX, y: centerPositionY)
    NSObject.cancelPreviousPerformRequests(withTarget: self)
    perform(#selector(UIScrollViewDelegate.scrollViewDidEndScrollingAnimation(_:)), with: nil, afterDelay: 0.3)
  }

  internal func readjustOffsets() {}
}
