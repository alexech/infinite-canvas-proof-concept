//
//  BoardLayer.swift
//  Yugen
//
//  Created by Aurora Rodríguez on 03/07/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class BoardLayer: CALayer, NSCopying {
  func copy(with zone: NSZone? = nil) -> Any {
    return BoardLayer()
  }

  var pathId: String = ""
  override init() {
    super.init()
  }
  override init(layer: Any) {
    super.init(layer: layer)
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
