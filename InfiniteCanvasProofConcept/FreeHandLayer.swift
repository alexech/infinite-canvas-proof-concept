//
//  FreeHandLayer.swift
//  Yugen
//
//  Created by Misael Pérez Chamorro on 5/28/18.
//  Copyright © 2018 Misael Pérez Chamorro. All rights reserved.
//

import UIKit

class FreeHandLayer: BoardLayer {

  var layerPath: [Pencil] = []
  var red: CGFloat = 0.0
  var green: CGFloat = 0.0
  var blue: CGFloat = 0.0
  var currentColor: UIColor = UIColor.black
  var brushWidth: CGFloat = 2.0
  var offset: CGPoint = CGPoint(x: 0, y: 0)

  override init() {
    super.init()
    self.pathId = ""
    zPosition = 4
  }

  override init(layer: Any) {
    super.init(layer: layer)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func draw(in ctx: CGContext) {
    print("FreeHandLayer draw")
    for pencilPoint in layerPath {
      let path = UIBezierPath()
      path.move(to: pencilPoint.location.start)
      path.addLine(to: pencilPoint.location.end)
      //lastPoint = pencilPoint.location.start
      let shapeLayer = CAShapeLayer()
      shapeLayer.path = path.cgPath
      shapeLayer.strokeColor = pencilPoint.color
      shapeLayer.lineWidth = pencilPoint.strokeWith
      shapeLayer.lineCap = convertToCAShapeLayerLineCap("round")
      addSublayer(shapeLayer)
    }
  }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertToCAShapeLayerLineCap(_ input: String) -> CAShapeLayerLineCap {
	return CAShapeLayerLineCap(rawValue: input)
}
