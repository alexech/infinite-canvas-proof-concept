//
//  VirtualLayer.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/13/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

class VirtualLayer: CALayer {
  
  var pencils: [Pencil] = []
  
  override init() {
    super.init()
  }
  
  override init(layer: Any) {
    super.init(layer: layer)
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  override func draw(in ctx: CGContext) {
    debugPrint("VirtualLayer pencils: ")
    debugPrint(pencils)
    for pencil in pencils {
      ctx.move(to: pencil.location.start)
      ctx.addLine(to: pencil.location.end)
      ctx.setLineCap(.round)
      ctx.setLineWidth(pencil.strokeWith)
      ctx.setStrokeColor(pencil.color)
      ctx.setBlendMode(.normal)
      ctx.strokePath()
    }
  }

}
