//
//  DrawImageView.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/12/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

class DrawCanvasView: UIView {

  weak var delegate: DrawCanvasViewDelegate?

  var brushWidth: CGFloat = 2.0
  var currentColor: UIColor = UIColor.black

  internal var pencils: [Pencil] = []
  internal var lastPoint = CGPoint.zero

  private let π = CGFloat.pi
  private let forceSensitivity: CGFloat = 4.0
  private let tiltThreshold = CGFloat.pi/5

  override public init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = .clear
  }

  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.backgroundColor = .clear
  }

  override open func draw(_ rect: CGRect) {
    guard let context: CGContext = UIGraphicsGetCurrentContext() else { return }
    for pencil in pencils {
      context.move(to: pencil.location.start)
      context.addLine(to: pencil.location.end)
      context.setLineCap(.round)
      context.setLineWidth(pencil.strokeWith)
      context.setStrokeColor(currentColor.cgColor)
      context.setBlendMode(.normal)
      context.strokePath()
    }
  }

  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else { return }
    lastPoint = touch.location(in: self)
    let pencilLocation = PencilLocation(start: lastPoint, end: lastPoint)
    savePencil(with: pencilLocation)
  }

  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else { return }
    if touch.altitudeAngle < tiltThreshold {
      brushWidth = lineWidthForShading(touch: touch)
    } else {
      brushWidth = lineWidthForDrawing(touch: touch)
    }
    let currentPoint = touch.location(in: self)
    let pencilLocation = PencilLocation(start: lastPoint, end: currentPoint)
    savePencil(with: pencilLocation)
    lastPoint = currentPoint
  }

  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    let layer = FreeHandLayer()
    layer.frame = self.bounds
    layer.currentColor = currentColor
    layer.layerPath = pencils
    delegate?.canvas(didFinishDrawing: layer)
    pencils = []
    self.setNeedsDisplay()
  }

  private func savePencil(with location: PencilLocation) {
    let pencil = Pencil(location: location , strokeWith: brushWidth, color: currentColor.cgColor)
    pencils.append(pencil)
    delegate?.canvas(isDrawing: pencil)
    self.setNeedsDisplay()
  }

  private func lineWidthForShading(touch: UITouch) -> CGFloat {
    let previousLocation = touch.previousLocation(in: self)
    let location = touch.location(in: self)
    let vector1 = touch.azimuthUnitVector(in: self)
    let vector2 = CGPoint(x: location.x - previousLocation.x, y: location.y - previousLocation.y)
    var angle = abs(atan2(vector2.y, vector2.x) - atan2(vector1.dy, vector1.dx))
    
    if angle > π {
      angle = 2 * π - angle
    }
    if angle > π / 2 {
      angle = π - angle
    }
    
    let minAngle: CGFloat = 0
    let maxAngle = π / 2
    let normalizedAngle = (angle - minAngle) / (maxAngle - minAngle)
    let maxLineWidth: CGFloat = 60
    let lineWidth = maxLineWidth * normalizedAngle
    return lineWidth
  }
  
  private func lineWidthForDrawing(touch: UITouch) -> CGFloat {
    if touch.force > 0 {
      let lineWidth = touch.force * forceSensitivity
      return lineWidth
    }
    return brushWidth
  }
}
