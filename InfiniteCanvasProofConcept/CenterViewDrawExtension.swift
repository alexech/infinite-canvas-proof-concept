//
//  CenterViewDrawExtension.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/13/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

extension CenterView {
  internal func allocate(layer: FreeHandLayer) {
    /*for pencil in layer.layerPath {
      debugPrint("------------------------------------------------")
      debugPrint("pencil location: \(pencil.location)")
      var mutablePencil = pencil
      mutablePencil.location.start.x += self.centerOffset.x
      mutablePencil.location.start.y += self.centerOffset.y
      mutablePencil.location.end.x += self.centerOffset.x
      mutablePencil.location.end.y += self.centerOffset.y
      debugPrint("mutablePencil location: \(mutablePencil.location)")
      self.allocatePencils.append(mutablePencil)
    }
    self.contentMode = .redraw
    let virtualLayer = VirtualLayer()
    virtualLayer.pencils = allocatePencils
    self.layer.addSublayer(virtualLayer)
    virtualLayer.setNeedsDisplay()
    self.layoutIfNeeded()*/
    //self.setNeedsDisplay()
    layer.position.x += self.centerOffset.x
    layer.position.y += self.centerOffset.y
    self.drawCenterView.layer.addSublayer(layer)
    layer.setNeedsDisplay()
  }
}
