//
//  DrawCanvasViewDelegate.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/13/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

protocol DrawCanvasViewDelegate: class {
  func canvas(isDrawing pencil: Pencil)
  func canvas(didFinishDrawing layer: FreeHandLayer)
}
