//
//  ViewController.swift
//  InfiniteCanvasProofConcept
//
//  Created by Alex Ech on 12/12/18.
//  Copyright © 2018 Alex Ech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var drawCanvasView: DrawCanvasView!
  @IBOutlet weak var centerView: CenterView!

  override func viewDidLoad() {
    super.viewDidLoad()
    initSetups()
  }
  
  @IBAction func pencilWasTouchedUpInside(_ sender: UIButton) {
    drawCanvasView.isUserInteractionEnabled = true
  }

  @IBAction func dragWasTouchedUpInside(_ sender: UIButton) {
    drawCanvasView.isUserInteractionEnabled = false
  }
  
  private func initSetups() {
    drawCanvasView.delegate = self
  }
}

extension ViewController: DrawCanvasViewDelegate {
  func canvas(didFinishDrawing layer: FreeHandLayer) {
    centerView.allocate(layer: layer)
  }

  func canvas(isDrawing pencil: Pencil) {
    // Is drawing
  }
}
